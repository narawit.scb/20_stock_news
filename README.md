For scrap news from website
Use Scrapy framework

## News Source
- อีไฟแนนซ์ไทย
- Hooninside
- HoonSmart
- ข่าวหุ้น
- ทันหุ้น
- มิติหุ้น
- infoquest
- Thai PR
- MGR Online
- ประชาชาติ
- กรุงเทพธุรกิจ

# Command for start crawl
Start crawl on specific web page via bash
``` 
scrapy crawl efinancethai 
```

Start Crawl via python scrip
```
python go_spider.py
```

# Docker Command
```
docker build -t stock_news .
docker run stock_news
docker start stock_news

## Run docker with attach bash
docker run -it --entrypoint bash stock_news
docker run -it --entrypoint bash --expose 3306 stock_news
docker run -it --entrypoint bash --net=host stock_news
```

# Help
[Scrapy](https://scrapy.org/)