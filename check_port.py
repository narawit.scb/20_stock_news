import socket
from contextlib import closing
import sys

def check_socket(host, port):
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
        if sock.connect_ex((host, port)) == 0:
            print("Port is open")
        else:
            print("Port is not open")

if __name__ == "__main__":
    host = sys.argv[1]
    port = sys.argv[2]
    check_socket(host, int(port))
