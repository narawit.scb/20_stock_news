from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from stock_news.spiders import KaohoonSpider

process = CrawlerProcess(get_project_settings())
process.crawl(KaohoonSpider)
process.start()

print("Process Complete")
