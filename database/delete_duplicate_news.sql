DELETE  t1 FROM scbs_web.news AS t1
INNER JOIN (

SELECT tmp.link, MAX(tmp.id) AS id
FROM scbs_web.news as tmp
GROUP BY tmp.link
HAVING COUNT(*) > 1

) AS t2 ON t1.link = t2.link
WHERE t1.id != t2.id;