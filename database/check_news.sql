SeLECT COUNT(*)
FROM scbs_web.news;

SELECT id, source, link, img_link, header, caption, title, content, publish_date, publish_time, last_update
FROM scbs_web.news
ORDER BY last_update DESC
LIMIT 10;


SELECT link, COUNT(*)
FROM scbs_web.news
GROUP BY link
HAVING COUNT(*) > 1;

