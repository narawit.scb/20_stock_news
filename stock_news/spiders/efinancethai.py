# -*- coding: utf-8 -*-
import scrapy
from ..config import config as config
from ..logger import logger as log
import re

class EfinancethaiSpider(scrapy.Spider):
    name = 'efinancethai'
    allowed_domains = ['efinancethai.com']
    allowed_news_link = "(.+www.efinancethai.com/LastestNews/LatestNewsMain.aspx\?ref=A&id=\w+)"
    #start_urls = ['http://efinancethai.com/']

    def start_requests(self):
        main_url = "http://www.efinancethai.com/LastestNews/controller_news_latest.aspx?key=getlastestnewslists_all&type=text&page_number=%d"
        for page in range(0,1400):
            yield scrapy.Request(url = main_url % page, callback = self.parse_news_link)

    def parse_news_link(self, response):
        link_news = response.xpath('//h2/a/@href').getall()
        if len(link_news) >= 1:
            for news in link_news:
                 target_link = response.urljoin(news[0:45])
                 news_link_pattern = re.compile(self.allowed_news_link)
                 check = news_link_pattern.match(target_link)
                 if check:
                    link = check.group(1)
                    yield scrapy.Request(link, meta={'deltafetch_key': link}, callback=self.parse_news_page)

    def parse_news_page(self, response):

        news_link = response.meta["deltafetch_key"]
        img_link = self.get_xpath(response, ['//meta[@id="linkimage"]/@content'])
        header = self.get_xpath(response, ['//meta[@id="metatitle"]/@content'])
        caption = self.get_xpath(response, ['//meta[@id="metadescshare"]/@content'])
        title_xpaths = ['//h1[@class="text_headcontent"]/text()', '//table[1]/tr[2]/text()']
        title = self.get_xpath(response, title_xpaths)
        content_xpaths = ['//table[1]/tr[4]/text()','//div/table/tr[1]/td/table/tr[3]','//div/table[@border=0]/tr[2]']
        content = self.get_xpath(response, content_xpaths)
        str_datetime = self.get_xpath(response, ['//div[@class="social"]/p[@class="share"]/text()[1]'])
        publish_date = str_datetime.split("|")[0].strip()
        publish_time = str_datetime.split("|")[1].strip()
        
        output = {
            'link': news_link,
            'img_link': img_link,
            'header': header,
            'caption': caption,
            'title': title,
            'content': content,
            'publish_date':publish_date,
            'publish_time':publish_time
        }

        ### Check output can capture all data
        is_pass = True
        for key,value in output.items():
            if len(value) < 1:
                log.warning(f"Craw {response.url} : error on {key} : value={value}")
                is_pass = False

        if is_pass:
            log.info(f"Craw <{response.url}> complete")
        else:
            log.warning(f"Craw <{response.url}> fault : {output}")

        yield output
        
    def get_xpath(self,response, title_xpaths, deafult = ""):
        output_text = deafult
        for xpath in title_xpaths:
            text = ""
            try:
                text = " ".join(response.xpath(xpath).getall())
            except Exception as e: 
                log.warning(f"get_xpath with {xpath}:{str(e)}")
            if len(text) > 5:
                break
        if len(text)>0:
            output_text = text
        return output_text.strip()


            
