# -*- coding: utf-8 -*-
import scrapy
from ..config import config as config
from ..logger import logger as log
import re

class HoonsmartSpider(scrapy.Spider):
    name = 'hoonsmart'
    allowed_domains = ['hoonsmart.com']
    allowed_news_link = "(.+www\.hoonsmart.com\/archives\/\d+)"
    exclude_field = []

    def start_requests(self):
        main_url = "https://www.hoonsmart.com/archives/category/breaking-news/page/%d"
        for page in range(1,10):
            yield scrapy.Request(url = main_url % page, callback = self.parse_news_link)

    def parse_news_link(self, response):
        link_news = response.xpath('//main[@id="main"]/article/div/div[@class="pic"]/a/@href').getall()
        if len(link_news) >= 1:
            for news in link_news:
                 target_link = news
                 news_link_pattern = re.compile(self.allowed_news_link)
                 check = news_link_pattern.match(target_link)
                 if check:
                    link = check.group(1)
                    yield scrapy.Request(link, meta={'deltafetch_key': link}, callback=self.parse_news_page)

    def parse_news_page(self, response):

        news_link = response.meta["deltafetch_key"]
        img_link = self.get_xpath(response, ['//meta[@property="og:image"]/@content'])
        header = self.get_xpath(response, ['//meta[@property="og:title"]/@content'])
        caption = self.get_xpath(response, ['//meta[@property="og:description"]/@content'])
        title = self.get_xpath(response, ['//h2[@class="main-title"]/a/text()'])
        content = self.get_xpath(response, ['//article/div[@class="entry-content"]/p/strong/text()|//article/div[@class="entry-content"]/p/text()'])
        datetime_str = self.get_xpath(response, ['//article/header[@class="entry-header"]/div[@class="entry-meta"]/span[@class="posted-on"]/a[@rel="bookmark"]/time[@class="entry-date published"]/text()'])
        publish_date = datetime_str.split(" ")[0]
        publish_time = datetime_str.split(" ")[1]
        
        output = {
            'link': news_link,
            'img_link': img_link,
            'header': header,
            'caption': caption,
            'title': title,
            'content': content,
            'publish_date':publish_date,
            'publish_time':publish_time
        }

        ### Check output can capture all data
        is_pass = True
        for key,value in output.items():
            if key in self.exclude_field:
                continue
            if len(value) < 1:
                log.warning(f"Craw {response.url} : error on {key} : value={value}")
                is_pass = False

        if is_pass:
            log.info(f"Craw <{response.url}> complete")
        else:
            log.warning(f"Craw <{response.url}> fault : {output}")

        yield output
        
    def get_xpath(self,response, title_xpaths, deafult = ""):
        output_text = deafult
        for xpath in title_xpaths:
            text = ""
            try:
                text = " ".join(response.xpath(xpath).getall())
            except Exception as e: 
                log.warning(f"get_xpath with {xpath}:{str(e)}")
            if len(text) > 5:
                break
        if len(text)>0:
            output_text = text
        return output_text.strip()


            
