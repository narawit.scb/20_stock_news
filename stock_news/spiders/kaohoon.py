# -*- coding: utf-8 -*-
import scrapy
from ..config import config as config
from ..logger import logger as log
import re
import xmltodict
from datetime import datetime
import pytz

class KaohoonSpider(scrapy.Spider):
    name = 'kaohoon'
    allowed_domains = ['kaohoon.com']
    allowed_news_link = "(.+www\.kaohoon\.com\/content/\d+)"
    exclude_field = []

    def start_requests(self):
        ### Change url to https://www.kaohoon.com/sitemap-news.xml for daily run
        if True:
            main_url = "https://www.kaohoon.com/sitemap-news.xml"
            yield scrapy.Request(url = main_url, callback = self.parse_news_link)
        else:
            main_url = "https://www.kaohoon.com/sitemap-posttype-post.%d%02.d.xml"
            # for year in reversed(range(2014, 2021)):
            #     for month in reversed(range(1,13)): 
            for year in reversed(range(2019, 2020)):
                for month in reversed(range(12,13)):       
                    yield scrapy.Request(url = main_url % (year,month), callback = self.parse_news_link)

    def parse_news_link(self, response):
        response_data = xmltodict.parse(response.body)
        for obj_link in response_data["urlset"]["url"]:
            ### When no data, Kaohoon will return default https://www.kaohoon.com/ as 1 object item that i will ignore
            if 'loc' not in obj_link:
                continue

            ### In normal case
            target_link = obj_link['loc']
            last_mod = ""
            if 'lastmod' in obj_link.keys():
                last_mod = obj_link['lastmod']
            else:
                last_mod = obj_link['news:news']["news:publication_date"]
            news_link_pattern = re.compile(self.allowed_news_link)
            check = news_link_pattern.match(target_link)
            if check:
                link = check.group(1)
                yield scrapy.Request(link, meta={'deltafetch_key': link, 'update':last_mod}, callback=self.parse_news_page)

    def parse_news_page(self, response):

        news_link = response.meta["deltafetch_key"]
        img_link = self.get_xpath(response, ['//meta[@property="og:image"]/@content'])
        header = self.get_xpath(response, ['//meta[@property="og:title"]/@content'])
        caption = self.get_xpath(response, ['//meta[@property="og:description"]/@content'])

        title = self.get_xpath(response, ['//header[@class="entry-header header"]/h1/text()'])
        content = self.get_xpath(response, ['//div[@class="content-main entry-content"]/p/descendant-or-self::text()', '//div[@class="content-main entry-content"]/table/tbody/descendant-or-self::text()'])
        datetime_utc = datetime.strptime(re.sub(r'(\+\d+):(\d+)', r'\1\2', response.meta["update"]), '%Y-%m-%dT%H:%M:%S%z')
        tz = pytz.timezone("Asia/Bangkok")
        thai_time = datetime_utc.astimezone(tz)
        publish_date = thai_time.strftime("%Y-%m-%d")
        publish_time = thai_time.strftime("%H:%m")
        
        output = {
            'link': news_link,
            'img_link': img_link,
            'header': header,
            'caption': caption,
            'title': title,
            'content': content,
            'publish_date':publish_date,
            'publish_time':publish_time
        }

        ### Check output can capture all data
        is_pass = True
        for key,value in output.items():
            if key in self.exclude_field:
                continue
            if len(value) < 1:
                log.warning(f"Craw {response.url} : error on {key} : value={value}")
                is_pass = False

        if is_pass:
            log.info(f"Craw <{response.url}> complete")
        else:
            log.warning(f"Craw <{response.url}> fault : {output}")

        yield output
        
    def get_xpath(self,response, title_xpaths, deafult = ""):
        output_text = deafult
        for xpath in title_xpaths:
            text = ""
            try:
                text = " ".join(response.xpath(xpath).getall())
            except Exception as e: 
                log.warning(f"get_xpath with {xpath}:{str(e)}")
            if len(text) > 5:
                break
        if len(text)>0:
            output_text = text
        return output_text.strip()


            
