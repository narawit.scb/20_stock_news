# -*- coding: utf-8 -*-
import scrapy
from ..config import config as config
from ..logger import logger as log
import re

class HooninsideSpider(scrapy.Spider):
    name = 'hooninside'
    allowed_domains = ['hooninside.com']
    allowed_news_link = "(.+www\.hooninside.com\/news-feed\/\d+\/view)"
    exclude_field = ['caption', 'content']
    #start_urls = ['http://efinancethai.com/']

    def start_requests(self):
        main_url = "https://www.hooninside.com/news-feed?page=%d"
        for page in range(0,1000):
            yield scrapy.Request(url = main_url % page, callback = self.parse_news_link)

    def parse_news_link(self, response):
        link_news = response.xpath('//table[@class="nf-tb"]/tbody/tr/td[4]/a/@href').getall()
        if len(link_news) >= 1:
            for news in link_news:
                 target_link = news
                 news_link_pattern = re.compile(self.allowed_news_link)
                 check = news_link_pattern.match(target_link)
                 if check:
                    link = check.group(1)
                    yield scrapy.Request(link, meta={'deltafetch_key': link}, callback=self.parse_news_page)

    def parse_news_page(self, response):

        news_link = response.meta["deltafetch_key"]
        img_link = self.get_xpath(response, ['//meta[@property="og:image"]/@content'])
        header = self.get_xpath(response, ['//meta[@property="og:title"]/@content'])
        caption = "" #TODO Find some caption
        title_xpaths = ['//div[@class="detail-title"]/h1/text()']
        title = self.get_xpath(response, title_xpaths)
        content_xpaths = ['//div[@class="detail-block"]/pre/br/following-sibling::text()',
            '//div[@class="detail-block"]/p/text()', '//div[@class="detail-news col-xs-12 nopadmobile"]/p/text()', '//pre[@class="newsstory-pre-font"]/text()']
        content = self.get_xpath(response, content_xpaths)
        publish_date = self.get_xpath(response, ['//div[@class="author"]/p/time/em/text()[1]'])
        publish_time = self.get_xpath(response, ['//div[@class="author"]/p/time/em/text()[2]'])
        
        output = {
            'link': news_link,
            'img_link': img_link,
            'header': header,
            'caption': caption,
            'title': title,
            'content': content,
            'publish_date':publish_date,
            'publish_time':publish_time
        }

        ### Check output can capture all data
        is_pass = True
        for key,value in output.items():
            if key in self.exclude_field:
                continue
            if len(value) < 1:
                log.warning(f"Craw {response.url} : error on {key} : value={value}")
                is_pass = False

        if is_pass:
            log.info(f"Craw <{response.url}> complete")
        else:
            log.warning(f"Craw <{response.url}> fault : {output}")

        yield output
        
    def get_xpath(self,response, title_xpaths, deafult = ""):
        output_text = deafult
        for xpath in title_xpaths:
            text = ""
            try:
                text = " ".join(response.xpath(xpath).getall())
            except Exception as e: 
                log.warning(f"get_xpath with {xpath}:{str(e)}")
            if len(text) > 5:
                break
        if len(text)>0:
            output_text = text
        return output_text.strip()


            
