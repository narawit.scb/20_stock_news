# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

from bs4 import BeautifulSoup
from .database import News, insert_news_db
import warnings
warnings.filterwarnings("ignore")

class StockNewsPipeline(object):
    def process_item(self, item, spider):
        for key in item.keys():
            item[key] = self.clean_html_tag(item[key])
        
        save_news = News(source=spider.name, link=item['link'], img_link=item['img_link'], header=item["header"], caption=item["caption"], title=item["title"], content=item["content"], publish_date=item["publish_date"], publish_time=item["publish_time"])
        insert_news_db([save_news])
        return item
    
    def clean_html_tag(self, text):
        return BeautifulSoup(text, "lxml").text.replace(u'\xa0','-').strip()
