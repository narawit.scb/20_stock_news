from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
import datetime
from ..logger import logger as log
from .engine import Engine

class News(declarative_base()):
    __tablename__ = 'news'

    id = Column(Integer, primary_key=True)
    source = Column(String)
    link = Column(String)
    img_link = Column(String)
    header = Column(String)
    caption = Column(String)
    title = Column(String)
    content = Column(String)
    publish_date = Column(String)
    publish_time = Column(String)
    last_update = Column(DateTime, default=datetime.datetime.utcnow)


    def __repr__(self):
        return "<News (link='%s', header='%s', caption='%s')>" % (self.link, self.header, self.caption)


def insert_news_db(list_news):
    
    try:
        session = Engine.getInstance().get_session()
        session.add_all(list_news)
        session.commit()
        
    except Exception as e:
        log.error(f"insert_news_db:{str(e)}")


def get_news_by_source(source):
    session = Engine.getInstance().get_session()
    list_news =  session.query(News).filter(News.source == source).all()
    return list_news

def get_crawled_link(source):
    session = Engine.getInstance().get_session()
    list_news =  session.query(News).filter(News.source == source).with_entities(News.link).all()
    return list_news