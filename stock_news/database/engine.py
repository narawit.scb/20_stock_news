from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from ..config import config as config
from ..logger import logger as log

class Engine():
    
    __instance = None
    __session = None
    __engine = None
    
    @staticmethod
    def getInstance():
        if Engine.__instance == None:
            Engine()
        return Engine.__instance 

    def __init__(self):
        if Engine.__instance == None:
            
            engine = create_engine(config["database"]["connection_string"])
            self._session = sessionmaker(bind=engine)
            self.__engine = engine
            Engine.__instance = self
        else:
            log.error("Database Engine Duplicate")

    def get_session(self):
        
        Session = sessionmaker(bind=self.__engine)
        session = Session()
        return session
        