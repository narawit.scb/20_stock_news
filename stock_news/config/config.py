from configparser import ConfigParser

config = ConfigParser()
config.read("files/setting/config.ini")

db_config = config["database"]
config["database"]["connection_string"] = f"mysql+pymysql://{db_config.get('user')}:{db_config.get('password')}@{db_config.get('hostname')}:{db_config.get('port')}/{db_config.get('name')}?charset=utf8"